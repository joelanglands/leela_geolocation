import os
import sys
import glob
sys.path.append('..')

import numpy as np

import geolocation as gl
import gl_testing.extract_pdws as ex

import geolocation as gl
try:
    from geolocation.mapplotting import MapPlotter
    map_plotting = True
except ImportError:
    map_plotting = False
    print('Basemap probably not installed so no map plots will be displayed!')

test_data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                             'test_data')

dunk_station = gl.StationGeoPoint(4, 50.86037, -3.2398, 'DUN')
hers_station = gl.StationGeoPoint(0, 50.89080,  0.3165, 'HER')
milf_station = gl.StationGeoPoint(1, 51.70880,  -5.0534, 'MIL')
tart_station = gl.StationGeoPoint(2, 58.26472,  26.4617, 'TAR')
watn_station = gl.StationGeoPoint(3, 53.00570, -1.25129, 'WAT')

station_dict = {'Dunkeswell': dunk_station,
                'Herstmonceux': hers_station,
                'Milford': milf_station,
                'Tartu': tart_station,
                'Watnall': watn_station}

def calc_rms(arr):
    """Function to calculated RMS of numpy array or python list."""
    _sum = 0
    for i in arr:
        _sum += i*i
    return np.sqrt(_sum/len(arr))

def load_test_waveforms():
    """Function that loads a set of 5 matched test PDWs 1 from each station.

    The PDWs are 'manually' put into the PDW class i.e I have looked at them and
    determined which samples should go into the classes themselves because the
    extract waveform module is pretty bad. Note that for the minimisation, PDWs
    are allowed to be different number of samples. They should be close to being
    the same number of samples though because otherwise they're blatently not
    from the same event!
    """
    test_wvfm_data = np.load(os.path.join(test_data_dir, 'test_waveforms.npy'))
    pdws = []
    for i in test_wvfm_data:
        pdws.append(gl.PeakDetectionWaveform(station_dict[i['nodename']],
                                             0,
                                             i['peaktime'],
                                             i['sampleinterval'],
                                             i['wvfmdata'][460:550],
                                             calc_rms(i['wvfmdata'][550:])))
    return pdws

def load_test_data():
    """Function to load a big list of test data using the extract_pdws module.
    
    This function extracts PDWs using the `extract_pdws` module from one minute
    of LEELA data from five stations. Note that the `extract_pdws` is rubbish so
    these PDWs will not minimise properly. Also note that I have used hardcoded
    RMS values to put into the PDW classes. These are estimates as a proper
    investigation into the signal error is needed.
    """
    dunk = os.path.join(test_data_dir, 'Wfs_180921_1500_DUN_09000_5000_25.npy')
    hers = os.path.join(test_data_dir, 'Wfs_180921_1500_HER_09000_5000_25.npy')
    milf = os.path.join(test_data_dir, 'Wfs_180921_1500_MIL_09000_5000_25.npy')
    tart = os.path.join(test_data_dir, 'Wfs_180921_1500_TAR_09000_5000_25.npy')
    watn = os.path.join(test_data_dir, 'Wfs_180921_1500_WAT_09000_5000_25.npy')

    PDWS = []

    PDWS += ex.extract_pdws(dunk, dunk_station, 25.81)
    PDWS += ex.extract_pdws(hers, hers_station, 22.56)
    PDWS += ex.extract_pdws(milf, milf_station, 25.57)
    PDWS += ex.extract_pdws(tart, tart_station, 27.21)
    PDWS += ex.extract_pdws(watn, watn_station, 27.03)

    PDWS.sort()

    print('-> Loaded in {} PDWS.'.format(len(PDWS)))
    return PDWS


def make_error_map():
    """Unfinished function to produce a contour plot for the errors.

    This function is unfinished due to the contour plotting functions
    not complying at all!
    """
    if map_plotting == False:
        print('No way to produce maps! Exiting.')
        return
    test_pdws = load_test_waveforms()

    lats = np.linspace(25, 70, 50)
    lons = np.linspace(-25, 45, 50)

    points = []

    for i in lats:
        for j in lons:
            points.append(gl.CGridGeoPoint(0, i, j))
    
    map_points = []
    _points = []
            
    for n, p in enumerate(points):
        for pdw in test_pdws:
            pdw.start_time = gl.calc_light_time(p, pdw.get_station())
        cgrid_min = gl.coarse_grid(test_pdws, setting='EU')
        fine_chi = gl.FineChiMinimiser(test_pdws, cgrid_min)
        fine_chi.minimise(False, 0)
        _points.append(fine_chi.get_result_point())
        try:
            major_65, minor_65, area_65 = fine_chi.get_error_ellipse_params()
        except ValueError:
            continue
        _lat, _lon = p.get_lat_lon()
        map_points.append(gl.CGridGeoPoint(0, _lat, _lon, chisq=major_65))

    print(len(map_points), '/', len(points))

    _map = gl.MapPlotter(loc='EU')
    #_map.plot_contour(map_points)
    _map.plot_chi_coarse(map_points, minmarker=False, alpha=0.25)
    #_map.add_points(points)
    _map.show_map()
    
if __name__=='__main__':
    make_error_map()
