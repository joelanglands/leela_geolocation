"""Module that attempts to extract PDWs from chunks in a very rough way.

This module is a first pass attempt at extracting PDWs from chunks of data that
are in peak-centred numpy array form.  It doesn't work well at all but is used
simply for testing purposes. A much better trigger system is needed to operate
the code to it's full potential.

All this code does is find the envelope of the signal using the absolute value
of the analytic signal. It then looks for peaks and troughs in the envelope
around the peak centre and attempts to extract the signal between two troughs 
but allowing for secondary peaks in the envelope.
"""

import os
import glob

import numpy as np
import scipy.signal as sig

import geolocation as gl

import matplotlib.pyplot as plt

def extract_pdws(filename, station, station_rms):
    data = np.load(filename)

    waveforms = []
    wvfm = None

    last_end = 0
    END = data.size
    n_chain = 0
    pdw_index = 0
    for n, chunk in enumerate(data):
        try:
            this_wvfm, last_end, chain, peak_time = extract_from_chunk(chunk,
                                                                       last_end)
        except IndexError:
            if wvfm is not None:
                waveforms.append(gl.PeakDetectionWaveform(station,
                                                          pdw_index,
                                                          start_time,
                                                          chunk['sampleinterval'],
                                                          wvfm,
                                                          station_rms))
                pdw_index += 1
            wvfm = None
            continue
        if wvfm is None:
            wvfm = this_wvfm
            start_time = peak_time
            continue
        if chain is False:
            waveforms.append(gl.PeakDetectionWaveform(station,
                                                      pdw_index,
                                                      start_time,
                                                      chunk['sampleinterval'],
                                                      wvfm,
                                                      station_rms))
            pdw_index += 1
            wvfm = this_wvfm
            start_time = peak_time
        else:
            n_chain += 1
            wvfm = np.append(wvfm, this_wvfm)
            
    print('Loaded in {} waveforms.'.format(len(waveforms)))
    return waveforms

def extract_from_chunk(chunk, end_time_last_wvfm):
    wvfm = chunk['wvfmdata']
    peak_time = chunk['peaktime']
    sample_time = chunk['sampleinterval']

    envelope = np.abs(sig.hilbert(wvfm))
    troughs, p = sig.find_peaks(-envelope, prominence=1)
    peaks, p = sig.find_peaks(envelope, prominence=[3, 10])

    trough_dict = {'+': troughs[troughs-511 > 0] - 511, '-': (troughs[troughs-511 < 0] - 511)*-1}
    peak_dict =  {'+': peaks[peaks-511 > 0] - 511, '-': (peaks[peaks-511 < 0] - 511)*-1}

    start_i, end_i, other_start = get_pdw_start_end_indices(trough_dict, peak_dict)
 
    start_times = [start_i*sample_time + peak_time, other_start*sample_time + peak_time]
    end_time = end_i*sample_time + peak_time

    if True not in [check_times(end_time_last_wvfm, x, sample_time) for x in start_times]:
        return wvfm[start_i+511: end_i+511], end_time, False, peak_time

    if check_times(end_time_last_wvfm, start_times[0], sample_time) is True:
        s_i = start_i+511
    elif check_times(end_time_last_wvfm, start_times[1], sample_time) is True:
        s_i = other_start+511
        
    return wvfm[s_i:end_i+511], end_time, True, peak_time

def check_times(time_one, time_two, sample_time):
    if (time_one > time_two - sample_time) and (time_one < time_two + sample_time):
        return True
    return False

def get_pdw_start_end_indices(trough_dict, peak_dict):
    output_indices = []
    for sign in ['+', '-']:        
        peak_arr = sorted(peak_dict[sign])[:2]
        trou_arr = sorted(trough_dict[sign])[:2]
        if len(peak_arr)==0:
            output_indices.append(trou_arr[0])
            if sign == '-':
                other_start = trou_arr[1]
        elif peak_arr[0] > trou_arr[0] and peak_arr[0] < trou_arr[1]:
            output_indices.append(trou_arr[1])
            if sign == '-':
                other_start = trou_arr[0]
        else:
            output_indices.append(trou_arr[0])
            if sign == '-':
                other_start = trou_arr[1]
    
    return output_indices[1]*-1, output_indices[0], other_start*-1

def calc_rms(arr):
    """Function to calculated RMS of numpy array or python list."""
    _sum = 0
    for i in arr:
        _sum += i*i
    return np.sqrt(_sum/len(arr))
