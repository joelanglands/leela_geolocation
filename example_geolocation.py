"""Example script that shows how the algorithm can be used.

This scipt gives two main examples: `example_single_minimisation` and
`example_gsam_minimisation`. These examples are basic functions showing
how the geolocation package can be used.

`example_single_minimisation` shows a single minimisation with five PDWs,
one from each detector.

`example_gsam_minimisation` shows an attempt at a full-ish operational
Global Strike Association Map (GSAM). This is explained further in Dan Tovey's
paper.  This example is very weak due to the problem with extracting PDWs
properly. (See README).

IMPORTANT NOTE
Note that in these examples for this test data, instead of the start time of the
of the PDW (the time of which the first sample occurs) being used, the peak time
is used for a few reasons: Firstly, the test data I received from Sven includes
the peak time of the waveform as part of the structured array making it easy.
Secondly, because the algorithm uses time differences anyway and in the case
where there is no dispersion (obviously not true but there may be very little)
the peak time of the PDWs should be offset from the start time by the same
amount in each detector.

Example:
    You can run this script with a `-g` option to run `example_gsam_minimisation`
    or a `-s` option to run `example_single_minimisation`::

        $ python example_geolocation.py -s
"""
import argparse

import numpy as np
import matplotlib.pyplot as plt

import geolocation as gl
try:
    from geolocation.mapplotting import MapPlotter
    map_plotting = True
except ImportError:
    map_plotting = False
    print('Basemap probably not installed so no map plots will be displayed!')
    
import gl_testing as gl_test

#Populate the StationGeoPoint classes with their latitude/longitudes and indices
hers_station = gl.StationGeoPoint(0, 50.89080,  0.3165, 'HER')
milf_station = gl.StationGeoPoint(1, 51.70880,  -5.0534, 'MIL')
tart_station = gl.StationGeoPoint(2, 58.26472,  26.4617, 'TAR')
watn_station = gl.StationGeoPoint(3, 53.00570, -1.25129, 'WAT')
dunk_station = gl.StationGeoPoint(4, 50.86037, -3.2398, 'DUN')

#Form list of stations for use in important functions
stations = [hers_station,
            milf_station,
            tart_station,
            watn_station,
            dunk_station]

def example_single_minimisation():
    """Function to demonstrate how to use the algorithm for a set of five PDWs.

    This function takes the five test PDWs and performs a minimisation on them
    using the `minimise_all` function. It then plots the resulting fix on a map
    using the `MapPlotter` class from the geolocation package. It also plots the
    65% and 95% error ellipse which can be seen by zooming in on the point.
    """
    pdws = gl_test.load_test_waveforms()
    OS_params = gl.calc_OS_params(stations)

    c_point, f_chi, _ = minimise_all(pdws, OS_params)
    #returns CGridGeoPoint and a FineChiMinimiser class and the pdws (discarded)

    #extract information from f_chi class
    f_point = f_chi.get_result_point()
    cov = f_chi.get_cov()
    cov95 = f_chi.get_95_cov()
    p_value = f_chi.get_p_value()
    f_chi.get_null_p_value()

    #Show the mean waveform for this event
    #This is what you get after the minimisation routine effectively overlays
    #the pdws and you take the average amplitude at each sample over every
    #PDW.
    mean_wvfm = f_chi.get_mean_wvfm()
    plt.plot(mean_wvfm, 'm-')
    plt.xlabel(r'Sample, $i$')
    plt.ylabel(r'Mean Amplitude, $\bar{y}_{i}$')
    plt.show()

    print('======================================')
    print('Coarse grid location:', c_point)
    print('Fine minimisation location:', f_point)
    print('P value:', p_value)
    print('Chi squared at minimum:', f_chi.get_chi())
    print('======================================')

    if map_plotting == True:
        _map = MapPlotter(loc='EU')
        _map.add_points(f_point, size=30, colour='m', marker='x', label='Fine min')
        _map.add_points(c_point, size=30, colour='r', marker='^', label='Coarse grid')
        _map.plot_error_ellipse(cov, f_point, colour='r', label='65% CL Ellipse')
        _map.plot_error_ellipse(cov95, f_point, colour='g', label='95% Cl Ellipse')
        _map.show_legend()
        _map.show_map()


def example_gsam_minimisation():
    """Example of GSAM running over peak centred waveforms.

    This function loads in the test data from gl_testing. This is only an
    example of how a GSAM could be constructed and operated using the
    test data.  It uses the very complex `match_pdws_time_window` function
    which only looks for matches for sets of five PDWS -- one from each
    station. It then plots the resulting points on a map. These points
    should really be ignored since the PDWs have not been extracted well at
    all. A new trigger system needs to be devised to operate the code well.
    """
    global stroke_index
    stroke_index = 1
    
    PDWS = gl_test.load_test_data()

    #Calculate important constants for the stations
    OS_params = gl.calc_OS_params(stations)
    t_max = gl.calc_t_max(stations)

    time = []
    for p in PDWS:
        time.append(p.get_start_time())
    time = np.array(time)

    points = []

    for n, pdw in enumerate(PDWS):
        print('=============================')
        test_pdws = []
        if pdw.get_stroke_index() != -1:
            print('PDW already matched! Moving on...')
            continue
        init_time = pdw.get_start_time()
        end_time = init_time + t_max
        aux_arr = np.where((time >= init_time) & (time <= end_time))[0]
        cutoff = aux_arr[-1] + 1
        test_pdws = PDWS[n:cutoff]
        result = match_pdws_time_window(test_pdws, OS_params)
        if result is None:
            continue
        points.append(result[1].get_result_point())

    print('Matched events:', len(points))

    if map_plotting == True:
        _map = MapPlotter(loc='EU')
        _map.add_points(points, size=20, colour='m', marker='.')
        _map.show_map()
            
def minimise_all(pdws, OS_params, print_level=1):
    """Function that performs all minimisations on a set of PDWs

    Args:
        pdws (:obj:`list` of :obj:`PeakDetectionWaveform`): A list of pdws that
            the fill minimisation algorithm should be ran over.
        OS_params (:obj:`dict`): A dictionary of osculating sphere parameters
            required for the multilateration step.
        print_level (int): Should be set to 0, 1 or 2. Determines the verbosity
            of the MIGRAD function in the fine minimisation fit. 0 is quiet,
            1 prints information and 2 prints debugging info. Defaults to 1.
    Returns:
        CGridGeoPoint: The result point from the coarse grid minimisation.
        FineChiMinimiser: The FineChiMinimiser class from this fit. The whole
            class is returned rather than returning loads of values.
        List of PeakDetectionWaveforms: Returns the PDW objects passed to this
            function. This so that their stroke indices can set if certain
            criteria are met. (See `match_pdws_time_window` function).
    """
    try:
        ml_lat, ml_lon, ml_lat2, ml_lon2 = gl.multilateration(pdws, OS_params)
        guess_p1 = gl.BaseGeoPoint(ml_lat, ml_lon)
        guess_p2 = gl.BaseGeoPoint(ml_lat2, ml_lon2)
        cgrid_min = gl.coarse_grid_stage_zero(pdws, guess_p1, guess_p2, s0_err=3)
    except ValueError:
        cgrid_min = gl.coarse_grid(pdws, setting='EU')

    fine_chi = gl.FineChiMinimiser(pdws, cgrid_min)
    fine_chi.minimise(False, print_level)

    return cgrid_min, fine_chi, pdws

def match_pdws_time_window(_pdws, OS_params):
    """Function that attempts to match PDWs in the time window.

    This is an example of a function to match the PDWs in the time window. It
    only looks for one PDW in each detector so matches will always be in groups
    of five. A more robust and better function should be written to match
    different numbers of combinations. Also this relies on the p values being
    accurate which is only true if the PDWs are extracted properly.

    Args:
        _pdws (:obj:`list` of :obj:`PeakDetectionWaveform`): The list of PDWs in
            the time window.
        OS_params (:obj:`dict`): Dictionary of osculating sphere parameters.
    Returns:
        tuple (CGridGeoPoints, FineChiMinimiser): Returns the coarse grid point
            and the FineChiMinimiser object for a matched event. Returns `None`
            if no event is matched in this set of PDWs.
    """
    pdws = []
    for p in _pdws:
        if p.get_stroke_index() == -1: #Filter out matched pdws i.e those pdws
            pdws.append(p)             #whose stroke index IS NOT -1
    
    print('PDWS within time window:', len(pdws))
              
    pdws.sort()
    ref_pdw = pdws[0]
    ref_station = ref_pdw.get_station_index()
    
    station_ids = np.unique([x.get_station_index() for x in pdws])
    station_ids = station_ids[station_ids != ref_station]
    
    if len(station_ids) != 4:
        print('Not enough detectors!')
        return None
    
    pdw_st_1 = []
    pdw_st_2 = []
    pdw_st_3 = []
    pdw_st_4 = []
  
    for p in pdws[1:]:
        if p.get_station_index() == station_ids[0]:
            pdw_st_1.append(p)
        elif p.get_station_index() == station_ids[1]:
            pdw_st_2.append(p)
        elif p.get_station_index() == station_ids[2]:
            pdw_st_3.append(p)
        elif p.get_station_index() == station_ids[3]:
            pdw_st_4.append(p)

    N_arr = [len(pdw_st_1), len(pdw_st_2), len(pdw_st_3), len(pdw_st_4)]
    print('N_arr:', N_arr)
    print('Combinations:', np.prod(N_arr))

    pdw_combinations = []
    results = []
    
    for i in pdw_st_1:
        for j in pdw_st_2:
            for k in pdw_st_3:
                for l in pdw_st_4:
                    results.append(minimise_all([ref_pdw, i, j, k, l], OS_params, 0))

    min_chi = 99999
    min_result = None
    print('N results:', len(results))
    for r in results:  #Find set of PDWs with lowest chi squared
        if r[1].get_chi() < min_chi:
            min_result = r
            min_chi = r[1].get_chi()

    global stroke_index
            
    if min_result[1].get_p_value() > 0.05: #THIS IS THE CUT ON THE P VALUE
        for p in min_result[2]:
            p.set_stroke_index(stroke_index) #Set stroke index for pdw group
        print('Event matched! Stroke index: ', stroke_index)
        print('Result point:', min_result[1].get_result_point())
        stroke_index += 1
        return (min_result[0], min_result[1])
    else:
        print('Cannot match strokes')
        return None

if __name__=='__main__':
    parser = argparse.ArgumentParser(
        description='Example script demonstrating geolocation package')
    
    parser.add_argument('-s', '--single', help='Runs single minimisation',
                        action='store_true')
    parser.add_argument('-g', '--gsam', help='Runs example GSAM script',
                        action='store_true')
    
    args = parser.parse_args()
    if args.single is True:
        example_single_minimisation()
    if args.gsam is True:
        example_gsam_minimisation()
