"""Module to perform fast multilateration to estimate the location of strikes.

This module follows this paper:
https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.594.6212&rep=rep1&type=pdf
The two key functions are `calc_OS_params` and `multilateration`. The function
`calc_OS_params` should be used only once with the stations in use as it returns
import parameters for the `multilateration` function.
To enable the user to follow the paper if they wish, a lot of the parameter
names in functions mimic those in the paper and hence break regular parameter
naming conventions.

Attributes:
    EARTH_RADIUS: The radius of the Earth in metres
    a: Earth's semi-major axis in metres
    b: Earth semi-minor axis in metres
    e_sq: Earth's eccentricity squared

"""

import numpy as np
from scipy import linalg as la

from geolocation.geoutils import SPEED_OF_LIGHT

EARTH_RADIUS = 6371.0 * 1000.0
a = 6378137.0
b = 6356752.0
e_sq = 1 - (b**2 / a**2)

def _calc_arc_length(vec1, vec2):
    mag1 = la.norm(vec1)
    mag2 = la.norm(vec2)
    return np.arccos(np.dot(vec1, vec2) / (mag1 * mag2))

def _calc_K(M, X, Y):
    theta_mx = _calc_arc_length(M, X)
    theta_my = _calc_arc_length(M, Y)
    theta_xy = _calc_arc_length(X, Y)

    numerator = np.cos(theta_xy) - np.cos(theta_mx) * np.cos(theta_my)
    denominator = np.sin(theta_mx) * np.sin(theta_my)

    return np.arccos(numerator / denominator)

def _calc_ai(Pi, theta_mi):
    return (np.cos(Pi) - np.cos(theta_mi)) / np.sin(theta_mi)

def _calc_x_OS(lat, lon, R_OS):
    return R_OS * np.cos(lat) * np.cos(lon)

def _calc_y_OS(lat, lon, R_OS):
    return R_OS * np.cos(lat) * np.sin(lon)

def _calc_z_OS(lat, lon, R_OS):
    return R_OS * np.sin(lat)

def _calc_w_WGS(lat, lon=0):
    return (a * np.cos(lat)) / np.sqrt(1 - e_sq * (np.sin(lat)**2))

def _calc_z_WGS(lat, lon):
    return (a * (1 - e_sq) * np.sin(lat)) / (np.sqrt(1 - e_sq
                                                     * (np.sin(lat)**2)))

def _calc_x_WGS(lat, lon):
    return _calc_w_WGS(lat) * np.cos(lon)

def _calc_y_WGS(lat, lon):
    return _calc_w_WGS(lat) * np.sin(lon)

def _calc_ROS(lat):
    return np.sqrt(_calc_M0(lat) * _calc_N0(lat))

def _calc_M0(lat):
    #return (a*(1-e_sq))/(np.sqrt(1 - e_sq*(np.sin(lat)**2)))**3
    #This is what is written in the paper but it returns a daft result
    #-- Radius bigger than a and EARTH_RADIUS!
    #This below gives a sensible result instead.
    return (a * (1 - e_sq)) / (np.sqrt(1 - e_sq * (np.sin(lat)**2)))

def _calc_N0(lat):
    return a / (np.sqrt(1 - e_sq * (np.sin(lat)**2)))

def station_to_OS_coords(station, OS_params):
    """Function to convert stations coordinates to osculating sphere coords."""
    s_lat, s_lon = station.get_lat_lon()
    s_lat = np.radians(s_lat)
    s_lon = np.radians(s_lon)
    station_x = _calc_x_WGS(s_lat, s_lon) + OS_params['dx']
    station_y = _calc_y_WGS(s_lat, s_lon) + OS_params['dy']
    station_z = _calc_z_WGS(s_lat, s_lon) + OS_params['dz']

    return np.asarray([station_x, station_y, station_z])

def calc_OS_params(stations):
    """Function to compute important parameters of the oscuating sphere.

    This function should be used initially once using all stations that are in
    use. Once the osculating sphere parameters are calculated, they can be
    reused for each multilateration.

    Args:
        stations (:obj:`list` of :obj:`StationGeoPoint`): A list of
            StationGeoPoint objects corresponding to each station that is in
            use.

    Returns:
        dict: Dictionary of important osculating sphere parameters to be used
            in in the multilateration function.
    """
    station_coords = [s.get_lat_lon() for s in stations]
    sc_radians = [(np.radians(s[0]), np.radians(s[1])) for s in station_coords]

    xs = [_calc_x_WGS(*a) for a in sc_radians]
    ys = [_calc_y_WGS(*a) for a in sc_radians]
    zs = [_calc_z_WGS(*a) for a in sc_radians]
    ws = [_calc_w_WGS(*a) for a in sc_radians]

    tan_p_lon = np.arctan(np.mean(ys) / np.mean(xs))
    tan_p_lat = np.arctan(np.mean(zs) / (np.mean(ws)*(1 - e_sq)))

    R_OS = _calc_ROS(tan_p_lat)

    x2 = _calc_x_OS(tan_p_lat, tan_p_lon, R_OS)
    y2 = _calc_y_OS(tan_p_lat, tan_p_lon, R_OS)
    z2 = _calc_z_OS(tan_p_lat, tan_p_lon, R_OS)

    x1 = _calc_x_WGS(tan_p_lat, tan_p_lon)
    y1 = _calc_y_WGS(tan_p_lat, tan_p_lon)
    z1 = _calc_z_WGS(tan_p_lat, tan_p_lon)

    delta_X = x2 - x1
    delta_Y = y2 - y1
    delta_Z = z2 - z1

    return {
        'r': R_OS,
        'dx': delta_X,
        'dy': delta_Y,
        'dz': delta_Z,
        'tangLat': tan_p_lat,
        'tangLon': tan_p_lon
    }


def multilateration(pdws, OS_params):
    """Function that performs multilateration to produce two guess points.

    This function performs most of the maths from the paper mentioned above
    to produce two guess locations for the origin of the given pdws.
    It produces two points because of the two solutions to a quadratic equation.

    Args:
        pdws (:obj:`list` of :obj:`PeakDetectionWaveform`): A list of the pdws
            to find the guess locations for.
        OS_params (:obj:`dict`): A dictionary of osculating sphere parameters
            calculated by the `calc_OS_params` function.

    Returns:
        float: lat_1 -- the latitude of the first guess point.
        float: lon_1 -- the longitude of the first guess point.
        float: lat_2 -- the latitude of the second guess point.
        float: lon_2 -- the longitude of the second guess point.

    Raises:
        ValueError: If any of the resulting coordinates are NANs.
    """
    pdws.sort()

    X_coords = station_to_OS_coords(pdws[0].get_station(), OS_params)
    Y_coords = station_to_OS_coords(pdws[1].get_station(), OS_params)
    M_coords = station_to_OS_coords(pdws[2].get_station(), OS_params)

    K = _calc_K(M_coords, X_coords, Y_coords)

    sph_const = SPEED_OF_LIGHT/OS_params['r']
    Px = (pdws[2].get_start_time() - pdws[0].get_start_time()) * sph_const
    Py = (pdws[2].get_start_time() - pdws[1].get_start_time()) * sph_const

    theta_mx = _calc_arc_length(M_coords, X_coords)
    theta_my = _calc_arc_length(M_coords, Y_coords)
    theta_xy = _calc_arc_length(X_coords, Y_coords)

    ax = _calc_ai(Px, theta_mx)
    ay = _calc_ai(Py, theta_my)

    u1 = ax * np.cos(K) - ay
    u2 = ax * np.sin(K)
    u3 = ay * (np.sin(Px) / np.sin(theta_mx)) - ax * (np.sin(Py)
                                                      / np.sin(theta_my))

    cos_Bx_1 = (u3 * u1 + u2 * np.sqrt(u1**2 + u2**2 - u3**2)) / (u1**2
                                                                  + u2**2)
    cos_Bx_2 = (u3 * u1 - u2 * np.sqrt(u1**2 + u2**2 - u3**2)) / (u1**2
                                                                  + u2**2)

    for n, cos_Bx in enumerate((cos_Bx_1, cos_Bx_2)):
        theta_ms = np.arctan((np.cos(Px) - np.cos(theta_mx)) /
                             (np.sin(Px) + np.sin(theta_mx) * cos_Bx))
        theta_xs = theta_ms + Px
        theta_ys = theta_ms + Py

        A_mat = np.asarray([X_coords, Y_coords, M_coords])

        b1 = (OS_params['r']**2) * np.cos(theta_xs)
        b2 = (OS_params['r']**2) * np.cos(theta_ys)
        b3 = (OS_params['r']**2) * np.cos(theta_ms)
        b_vec = np.asarray([b1, b2, b3])

        s = np.dot(la.inv(A_mat), b_vec)

        offsets = np.array([OS_params['dx'], OS_params['dy'], OS_params['dz']])

        s = s - offsets

        if n == 0:
            lat_1 = np.degrees(np.arctan(s[2] / np.sqrt(s[0]**2 + s[1]**2)))
            lon_1 = np.degrees(np.arctan2(s[1], s[0]))
        elif n == 1:
            lat_2 = np.degrees(np.arctan(s[2] / np.sqrt(s[0]**2 + s[1]**2)))
            lon_2 = np.degrees(np.arctan2(s[1], s[0]))

    if np.any(np.isnan([lat_1, lat_2, lon_1, lon_2])):
        raise ValueError
        
    return lat_1, lon_1, lat_2, lon_2
