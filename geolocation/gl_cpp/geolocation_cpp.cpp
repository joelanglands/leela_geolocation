#include <cmath>
#include <string>

#include "geolocation_cpp.hpp"

double calc_distance(double lat1, double lon1, double lat2, double lon2, double dlayer){
  //latitude and longitude in DEGREES
  //dlayer should be in KILOMETRES (confusing somewhat, may change)
  double d2r = M_PI/180.0; //degrees to radians constant

  double Dlat = fabs(lat1*d2r - lat2*d2r);
  double Dlon = fabs(lon1*d2r - lon2*d2r);

  double asin_arg = sqrt(sin(Dlat/2.0)*sin(Dlat/2.0)
						 +cos(lat1*d2r)*cos(lat2*d2r)*sin(Dlon/2.0)*sin(Dlon/2.0));

  double great_circle = 2.0*EARTH_RADIUS*asin(asin_arg);

  return sqrt(great_circle*great_circle + 4*(dlayer*1000)*(dlayer*1000));

}

double calc_light_time(double lat1, double lon1, double lat2, double lon2, double dlayer){
  double distance = calc_distance(lat1, lon1, lat2, lon2, dlayer);
  return distance/SPEED_OF_LIGHT;
}

double calc_t0_shift(pdw ref_pdw, pdw other_pdw, double grid_lat, double grid_lon, double dlayer){
  double dt_ref = calc_light_time(ref_pdw.st_lat, ref_pdw.st_lon, grid_lat, grid_lon, dlayer);
  double dt_other = calc_light_time(other_pdw.st_lat, other_pdw.st_lon, grid_lat, grid_lon, dlayer);
  return ref_pdw.start_time + dt_other - dt_ref;
}

double calc_alpha(double t0_shift, double t0, double t_samp){
  return (t0_shift - t0)/t_samp;
}

int calc_ts_index(double t0_shift, double t0, double t_samp){
  return int(floor(calc_alpha(t0_shift, t0, t_samp))); //floor returns a float ??
}

double calc_chisq(pdw ref_pdw, std::vector<pdw> other_pdws, std::tuple<double, double> grid_point, double dlayer){
  double sum = 0;
  double gLat = std::get<0>(grid_point);
  double gLon = std::get<1>(grid_point);
  dArray ref_wvfm = ref_pdw.wvfm;

  for(std::vector<pdw>::iterator it = other_pdws.begin(); it != other_pdws.end(); ++it){
	double t_shift = calc_t0_shift(ref_pdw, *it, gLat, gLon, dlayer);

	int i_prime = calc_ts_index(t_shift, it->start_time, ref_pdw.sample_time);

	dArray other_wvfm = it->wvfm;

	int N_sample = static_cast<int>(ref_wvfm.size());
	int other_pdw_len = static_cast<int>(other_wvfm.size());

	double sigma = (ref_pdw.sigma)*(ref_pdw.sigma) + (it->sigma)*(it->sigma);

	int i = 0;
	while(i < N_sample){
	  double other_y = 0;
	  double ref_y = ref_wvfm[i];
	  if(i_prime + 1 < other_pdw_len && i_prime > 0){
		other_y = other_wvfm[i_prime];
	  }
	  else {
		other_y = 0;
	  }

	  sum += ((ref_y - other_y)*(ref_y - other_y))/sigma;
	  ++i_prime;
	  ++i;
	} //end of while loop over samples in ref and other wvfm
  } // end of for loop over other pdws
  return sum;
}

pdw parse_pdw(py::dict _pdw){
  pdw this_pdw;
  this_pdw.start_time = py::cast<double>(_pdw["start_time"]);
  this_pdw.st_lat = py::cast<double>(_pdw["st_lat"]);
  this_pdw.st_lon = py::cast<double>(_pdw["st_lon"]);
  this_pdw.sigma = py::cast<double>(_pdw["sigma"]);
  this_pdw.sample_time = py::cast<double>(_pdw["sample_time"]);
  this_pdw.wvfm = py::cast<dArray>(_pdw["wvfm"]);
  return this_pdw;
}

cGrid generate_coarse_grid(py::dict gridDict){
  double lat_start = py::cast<double>(gridDict["lat_start"]);
  double lat_end = py::cast<double>(gridDict["lat_end"]);
  double lat_step = py::cast<double>(gridDict["lat_step"]);
  double lon_start = py::cast<double>(gridDict["lon_start"]);
  double lon_end = py::cast<double>(gridDict["lon_end"]);
  double lon_step = py::cast<double>(gridDict["lon_step"]);
  cGrid coarse_grid;
  for(double lat = lat_start; lat <= lat_end; lat += lat_step){
	for(double lon = lon_start; lon <= lon_end; lon += lon_step){
	  coarse_grid.push_back(std::make_tuple(lat, lon));
	} //end longitude loop
  } //end latitude loop
  return coarse_grid;
}

py::tuple coarse_grid(py::dict gridDict, py::dict ref_pdw_dict, py::list other_pdws, double dlayer){
  cGrid coarse_grid = generate_coarse_grid(gridDict);

  pdw ref_pdw = parse_pdw(ref_pdw_dict);
  std::vector<pdw> other_pdws_vec;

  for(py::handle i : other_pdws){
	other_pdws_vec.push_back(parse_pdw(py::cast<py::dict>(i)));
  }

  double min_chi_sq = 999999.0;
  std::tuple<double, double> min_l;

  //Hyperbola cut parameters
  double delta_hyp = (other_pdws_vec[0].start_time - ref_pdw.start_time)*SPEED_OF_LIGHT;
  double delta = ref_pdw.sample_time*HYPERBOLA_SAMPLE_ERROR*SPEED_OF_LIGHT;

  int within_hyp_count = 0;
  
  for(cGrid::iterator l = coarse_grid.begin(); l != coarse_grid.end(); ++l){
	double this_chisq = 100000;
	double gLat = std::get<0>(*l);
	double gLon = std::get<1>(*l);
	double d2 = calc_distance(gLat, gLon, ref_pdw.st_lat, ref_pdw.st_lon, 70);
	double d1 = calc_distance(gLat, gLon, other_pdws_vec[0].st_lat, other_pdws_vec[0].st_lon, 70);
	double DT = fabs(d1 - d2);
	if(d1 > d2 && DT >= (delta_hyp - delta) && DT <= (delta_hyp + delta)){ //hyperbola cut
	  ++within_hyp_count;
	  this_chisq = calc_chisq(ref_pdw, other_pdws_vec, *l, dlayer);
	} else {
	  this_chisq = 1000000; //set to arbitrarily high value if not within hyperbola
	}
	if(this_chisq < min_chi_sq){
	  min_l = *l;
	  min_chi_sq = this_chisq;
	}
  } //end of for loop over coarse grid
  //return py::make_tuple(x, y, a, sum_vec(vec));
  return py::make_tuple(std::get<0>(min_l), std::get<1>(min_l), min_chi_sq, within_hyp_count);
}

py::tuple coarse_grid_stage_zero(py::dict gridDict_1, py::dict gridDict_2, py::dict ref_pdw_dict, py::list other_pdws, double dlayer){
  cGrid cgrid_1 = generate_coarse_grid(gridDict_1);
  cGrid cgrid_2 = generate_coarse_grid(gridDict_2);

  pdw ref_pdw = parse_pdw(ref_pdw_dict);
  std::vector<pdw> other_pdws_vec;

  for(py::handle i : other_pdws){
	other_pdws_vec.push_back(parse_pdw(py::cast<py::dict>(i)));
  }
  
  double min_chi_sq = 999999.0;
  std::tuple<double, double> min_l;

  int within_box_count = 0;
  int total_count = 0;
  
  for(cGrid::iterator l = cgrid_1.begin(); l != cgrid_1.end(); ++l){
	double this_chisq = 1000000;
	++within_box_count;
	++total_count;
	this_chisq = calc_chisq(ref_pdw, other_pdws_vec, *l, dlayer);
	if(this_chisq < min_chi_sq){
	  min_l = *l;
	  min_chi_sq = this_chisq;
	}
  } //end of for loop over coarse grid one

  double lat_start = py::cast<double>(gridDict_1["lat_start"]);
  double lat_end = py::cast<double>(gridDict_1["lat_end"]);
  double lon_start = py::cast<double>(gridDict_1["lon_start"]);
  double lon_end = py::cast<double>(gridDict_1["lon_end"]);
  
  for(cGrid::iterator l = cgrid_2.begin(); l != cgrid_2.end(); ++l){
	double this_chisq = 100000;
	double gLat = std::get<0>(*l);
	double gLon = std::get<1>(*l);
	++total_count;

	if(gLat > lat_start && gLat < lat_end && gLon > lon_start && gLon < lon_end){
	  this_chisq = 1000000;
	} else {
	  ++within_box_count;
	  this_chisq = calc_chisq(ref_pdw, other_pdws_vec, *l, dlayer);
	}
	if(this_chisq < min_chi_sq){
	  min_l = *l;
	  min_chi_sq = this_chisq;
	}	
  } //end of for loop over coarse grid two
  return py::make_tuple(std::get<0>(min_l), std::get<1>(min_l), min_chi_sq, within_box_count);
}

double fine_min_func(py::dict ref_pdw_dict, py::list other_pdws, double lat, double lon, double dlayer){
  pdw ref_pdw = parse_pdw(ref_pdw_dict);
  std::vector<pdw> other_pdws_vec;

  for(py::handle i : other_pdws){
	other_pdws_vec.push_back(parse_pdw(py::cast<py::dict>(i)));
  }

  double sum = 0;

  for(std::vector<pdw>::iterator it = other_pdws_vec.begin(); it != other_pdws_vec.end(); ++it){
	double t_shift = calc_t0_shift(ref_pdw, *it, lat, lon, dlayer);

	int i_prime = calc_ts_index(t_shift, it->start_time, ref_pdw.sample_time);

	double alpha = calc_alpha(t_shift, it->start_time, ref_pdw.sample_time);

	int N_sample = static_cast<int>(ref_pdw.wvfm.size());
	int other_pdw_len = static_cast<int>(it->wvfm.size());

	double sigma = (ref_pdw.sigma)*(ref_pdw.sigma) + (it->sigma)*(it->sigma);
	
	int i = 0;
	double other_y = 0;
	
	while(i < N_sample){
	  double ref_y = ref_pdw.wvfm[i];
	  
	  if(i_prime + 1 < other_pdw_len && i_prime >= 0){
		other_y = interpolate_wvfm(it->wvfm[i_prime], it->wvfm[i_prime + 1], alpha, i_prime, N_sample);
	  } else {
		other_y = 0;
	  }
		  	  
	  sum += ((ref_y - other_y)*(ref_y - other_y))/sigma;
	  ++i_prime;
	  ++i;
	} //end of while loop over samples in ref and other wvfm
  } // end of for loop over other pdws
  return sum;
}

double interpolate_wvfm(double low_p, double hi_p, double alpha, int i_prime, int n_samp){
  //To make the first set of if statements work
  double lo_p_loc = low_p;
  double hi_p_loc = hi_p;
  //declare variables to be used
  double mult = 0;
  double rhs = 0;
  
  if(i_prime < 0 || i_prime > (n_samp - 1)){
	lo_p_loc = 0;
  }
  if(i_prime < -1 || i_prime > (n_samp - 2)){
	hi_p_loc = 0;
  }
  
  if(int(floor(alpha)) == 0){
	mult = alpha;
  }	else if(floor(alpha) > 0.0){
	mult = remainder(alpha, floor(alpha));
  } else if(alpha < 0 && alpha > -1){
	mult = 1 - fabs(alpha);
  } else if(floor(alpha) < 0){
	mult = remainder(alpha, ceil(alpha)) + 1.0;
  }
  
  rhs = (hi_p_loc - lo_p_loc)*mult;

  return (lo_p_loc + rhs);
}

PYBIND11_MODULE(geolocation_cpp, m){
  m.doc() = "The C++ geolocation library. Includes functions that are required to be quickly computed.\nThis library shouldn't really be interacted with much directly. Instead use the python driving functions in geoutils.py";
  m.def("coarse_grid", &coarse_grid);
  m.def("coarse_grid_stage_zero", &coarse_grid_stage_zero);
  m.def("calc_distance", &calc_distance);
  m.def("fine_min_func", &fine_min_func);
  m.def("interpolate_wvfm", &interpolate_wvfm);
}
