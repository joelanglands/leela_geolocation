#ifndef GEOLOCATION_HPP
#define GEOLOCATION_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>

#define EARTH_RADIUS 6371008.0
#define SPEED_OF_LIGHT 299792458.0
#define HYPERBOLA_SAMPLE_ERROR 10
/*This is for the hyperbola cut in the coarse grid. It defines the
 'width' of the search hyperbola.*/

namespace py = pybind11;

//typedefs to abbreviate commonly used types
typedef std::vector<double> dArray;
typedef std::vector<std::tuple<double, double>> cGrid;

struct pdw{
  double start_time;
  double st_lat;
  double st_lon;
  double sigma;
  double sample_time;
  dArray wvfm;
};

//function prototypes
double calc_distance(double lat1, double lon1, double lat2, double lon2, double dlayer);

double calc_light_time(double lat1, double lon1, double lat2, double lon2, double dlayer);

double calc_t0_shift(pdw ref_pdw, pdw other_pdw, double grid_lat, double grid_lon, double dlayer);

int calc_ts_index(double t0_shift, double t0, double t_samp);

double calc_alpha(double t0_shift, double t0, double t_samp);

double calc_chisq(pdw ref_pdw, std::vector<pdw> other_pdws, std::tuple<double, double> grid_point, double dlayer);

pdw parse_pdw(py::dict _pdw);

dArray list_to_vector(py::list list);

cGrid generate_coarse_grid(py::dict gridDict);

py::tuple coarse_grid(py::dict gridDict, py::dict ref_pdw_dict, py::list other_pdws, double dlayer);

py::tuple coarse_grid_stage_zero(py::dict gridDict_1, py::dict gridDict_2, py::dict ref_pdw_dict, py::list other_pdws, double dlayer);

double fine_min_func(py::dict ref_pdw_dict, py::list other_pdws, double lat, double lon, double dlayer);

double interpolate_wvfm(double low_p, double hi_p, double alpha, int i_prime, int n_samp);

#endif /* GEOLOCATION_HPP */
