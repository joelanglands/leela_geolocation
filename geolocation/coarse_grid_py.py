##################################################################################
#
#           This code is here as legacy code. It runs way too slowly to be useful
#           but is here for completeness
#           This will also need the correct imports writing in to work
#
#
#################################################################################

from geoclasses import BaseGeoPoint, CGridGeoPoint, StationGeoPoint
from geoutils import calc_distance, calc_light_time, calc_t0_shift, calc_ts_index

class CoarseGrid(object):
    def __init__(self, lat_start=10, lat_end=70, lat_step=0.05, lon_start=-25, \
                 lon_end=40, lon_step=0.05):
        self.coarse_grid = []
        count = 0
      
        for lat in np.arange(lat_start, lat_end, lat_step):
            for lon in np.arange(lon_start, lon_end, lon_step):
                self.coarse_grid.append(CGridGeoPoint(count, lat, lon))
                count += 1
        self.n_lats = len(np.arange(lat_start, lat_end, lat_step))
        self.n_lons = len(np.arange(lon_start, lon_end, lon_step))
        self.centre_lat = (lat_end + lat_start)/2.0
        self.centre_lon = (lon_end + lon_start)/2.0
        self.grid_size = len(self.coarse_grid)
        self.calculated = False
        self.min_grid_point = None

    @classmethod
    def from_preset(cls, setting='EU', lat_step=0.05, lon_step=0.05):
        lat_start = preset_map_settings[setting.upper()]['llclat']
        lat_end = preset_map_settings[setting.upper()]['urclat']
        lon_start = preset_map_settings[setting.upper()]['llclon']
        lon_end = preset_map_settings[setting.upper()]['urclon']
        return cls(lat_start, lat_end, lat_step, lon_start, lon_end, lon_step)

    def plot_pdws_atmin(self, pdws):
        if self.min_grid_point == None:
            print('No min_grid_point! Run calc_chi_grid() first!')
            return
        pdws.sort()
        ref_pdw = pdws[0]
        other_pdws = pdws[1:]
        plt.plot(ref_pdw.get_wvfm(), label=ref_pdw.get_station().get_id())
        for other in other_pdws:
            t_shift = calc_t0_shift(ref_pdw.get_start_time(), ref_pdw.get_station(), \
                                    other.get_station(), self.min_grid_point, 70)
            i_prime = calc_ts_index(t_shift, other.get_start_time(), ref_pdw.get_sample_time())
            if i_prime < 0:
                _zeros = [0]*abs(i_prime)
                _data = _zeros + other.get_wvfm()
                plt.plot(_data, label=other.get_station().get_id())
            elif i_prime > 0:
                _data = other.get_wvfm()[i_prime:]
                plt.plot(_data, label=other.get_station().get_id())
            else:
                plt.plot(other.get_wvfm(), label=other.get_station().get_id())
        plt.legend()
        plt.xlabel(r'Sample $i$')
        plt.ylabel(r'$\tilde{y}(i,j,k)$ [a.u]')
        plt.title('Aligned PDWs from coarse grid')
        plt.xlim(0, len(ref_pdw))
        plt.show()
            
    def __getitem__(self, index):
        return self.coarse_grid[index]

    def __len__(self):
        return self.grid_size

    def __str__(self):
        return '<CoarseGrid: {}lat x {}lon - centred @ ({:.3f}, {:.3f})>'.format(self.n_lats, \
                                                                               self.n_lons, \
                                                                               self.centre_lat, \
                                                                               self.centre_lon)
    def __repr__(self):
        return self.__str__()
    
    def reset(self):
        for point in self.coarse_grid:
            point.set_chisq(None)
        self.calculated = False


    def calc_chi_grid(self, pdws):
        pdws.sort()
        ref_pdw = pdws[0]
        other_pdws = pdws[1:]
        min_chisq = 99999999.0

        delta = ref_pdw.get_sample_time()*2*SPEED_OF_LIGHT
        delta_T = (other_pdws[0].get_start_time() - ref_pdw.get_start_time())
        delta_hyp = delta_T*SPEED_OF_LIGHT
        delta_up = np.sqrt((delta_T**2)*(SPEED_OF_LIGHT**2) + 4*(90000**2))

        for l in self.coarse_grid:
            #d2 = calc_distance(l, ref_pdw.get_station())
            #d1 = calc_distance(l, other_pdws[0].get_station())
            
            #DT = abs(d1 - d2)
            #if d1 > d2 and DT >= delta_hyp - delta and DT <= delta_hyp + delta:
            #this_chisq = self._calc_chisq(ref_pdw, other_pdws, l)
            #else:
             #   this_chisq = 100000

            if self._hyperbola_check(l, ref_pdw, other_pdws) == True:
                this_chisq = self._calc_chisq(ref_pdw, other_pdws, l)
            else:
                this_chisq = 100000
            
            l.set_chisq(this_chisq)
            if this_chisq < min_chisq:
                self.min_grid_point = l
                min_chisq = this_chisq

        self.calculated = True
        return self.get_min_grid_point()

        
    def get_min_grid_point(self):
        if self.calculated == True:
            return self.min_grid_point
        else:
            min_chi = 999999999
            for l in self.coarse_grid:
                if l.get_chisq() < min_chi:
                    self.min_grid_point = l
                    min_chi = l.get_chisq()
            return self.min_grid_point

    def _calc_chisq(self, ref_pdw, other_pdws, grid_point):
        _sum = 0
        for other_pdw in other_pdws:
            t_shift = calc_t0_shift(ref_pdw.get_start_time(), ref_pdw.get_station(), \
                                    other_pdw.get_station(), grid_point, 70)
            i_prime = calc_ts_index(t_shift, other_pdw.get_start_time(), ref_pdw.get_sample_time())
            alpha = (t_shift - other_pdw.get_start_time())/ref_pdw.get_sample_time()

            ref_wvfm = ref_pdw.get_wvfm()
            N_sample = len(ref_pdw)
            other_wvfm = other_pdw.get_wvfm()
            i = 0
            sigma = ref_pdw.get_sigma()**2 + other_pdw.get_sigma()**2

            while i < N_sample:
                ref_y = ref_wvfm[i]
                if i_prime+1 < len(other_wvfm) and i_prime > 0:
                    other_y = other_wvfm[i_prime]
                else:
                    other_y = 0

                _sum += ((ref_y - other_y)**2)/sigma
                i += 1
                i_prime += 1
                
        return _sum

    def _hyperbola_check(self, l, ref_pdw, other_pdws):
        check_array = []
        ref_start = ref_pdw.get_start_time()
        hyp_err = ref_pdw.get_sample_time()*2*SPEED_OF_LIGHT

        for other in other_pdws:
            delta_T = other.get_start_time() - ref_pdw.get_start_time()
            delta_lo = delta_T*SPEED_OF_LIGHT
            delta_hi = np.sqrt((delta_T**2)*(SPEED_OF_LIGHT**2) + 4*(90000**2))
            d2 = calc_distance(l, ref_pdw.get_station())
            d1 = calc_distance(l, other.get_station())
            DT = abs(d1 - d2)
            if d1 > d2 and DT >= delta_lo - hyp_err and DT <= delta_hi + hyp_err:
                check_array.append(True)
            else:
                check_array.append(False)

        return np.all(check_array)
