"""Class to plot maps using basemap.

Note that this module requires re-writing in Cartopy to be future proof as
basemap will be no longer supported.
"""

import os
import sys

import numpy as np
import scipy.linalg as la
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import pyproj
from matplotlib.patches import Polygon
import matplotlib.pyplot as plt
import matplotlib.colors as colours

from geolocation.geoutils import preset_map_settings

class MapPlotter(Basemap):
    def __init__(self, loc='EU', res='i', colour=None):
        ms = preset_map_settings[loc.upper()]
        super().__init__(llcrnrlon=ms['llclon'], llcrnrlat=ms['llclat'],urcrnrlon=ms['urclon'], \
                         urcrnrlat=ms['urclat'], resolution=res, projection='mill')
        self._fig = plt.figure(figsize=(12.8, 9.6))
        self._fig.set_tight_layout(True)
        self.drawcoastlines()
        #self.drawparallels(np.arange(ms['llclat'], ms['urclat'], 5))
        #self.drawmeridians(np.arange(ms['llclon'], ms['urclon'], 5))

        if colour == True:
            self.colour_map()

    def __del__(self):
        plt.close(self._fig)

    def colour_map(self, land_col='green', sea_col='aqua'):
        self.fillcontinents(color=land_col, lake_color='aqua', alpha=0.5)
        self.drawmapboundary(fill_color=sea_col)

    
    def show_map(self):
        plt.show()

    def add_title(self, title_str):
        plt.title(title_str)

    def save_map(self, save_as=None):
        if type(save_as) == type('string'):
            root, ext = os.path.splitext(save_as)
            if not ext:
                ext = '.png'
            plt.savefig(root + ext)
            print('Saved map as:', root+ext)
        else:
            plt.savefig('default_map_saveas.png')
            print('Saved map as: default_map_saveas.png')

    def show_legend(self):
        ax = plt.gca()
        handles, labels = ax.get_legend_handles_labels()
        if len(labels) == 0:
            return
        else:
            plt.legend()

    def add_points(self, points, colour='r', size=5, marker='o', label=None):
        if type(points) != list:
            lats, lons = points.get_lat_lon()
        else:
            lats, lons = [], []
            for p in points:
                _lat, _lon = p.get_lat_lon()
                lats.append(_lat)
                lons.append(_lon)

        self.scatter(lons, lats, s=size, marker=marker, color=colour, \
                     latlon=True, label=label, zorder=4)

    def add_points_dual(self, lats, lons, colour='r', size=5, marker='o', label=None):
        #I would like to combine this function into the one above for compactness and ease of use
        self.scatter(lons, lats, s=size, marker=marker, c=colour, cmap='viridis', \
                     latlon=True, label=label, zorder=4)

    def plot_chi_coarse(self, chisq_grid, size=15, alpha=0.05, minmarker=True):
        lats, lons = [], []
        chisqs = []
        minlat, minlon, minchi = 0, 0, 99999
        for point in chisq_grid:
            _lat, _lon = point.get_lat_lon()
            lats.append(_lat)
            lons.append(_lon)
            chisqs.append(point.get_chisq())
            if point.get_chisq() < minchi:
                minlat = _lat
                minlon = _lon
                minchi = point.get_chisq()

        normalize = colours.Normalize(min(chisqs), max(chisqs))
        self.scatter(lons, lats, s=size, marker='o', c=chisqs, cmap='viridis', latlon=True, \
                     alpha=alpha, zorder=3)
        self.cmap = plt.colorbar()
        self.cmap.set_label(r'$\chi^{2}$', fontsize=12)
        self.cmap.solids.set_alpha(1)
        if minmarker == True:
            self.scatter([minlon], [minlat], s=30, color='k', marker='8', latlon=True, zorder=3)


    def plot_error_ellipse(self, covar, ellip_centre, colour='m', label='Error Ellipse'):
        #helper function for below
        centre_lat, centre_lon = ellip_centre.get_lat_lon()

        theta = 0.5*np.arctan(2*covar[0][1]/(covar[1][1]-covar[0][0]))
        SQRT = np.sqrt((covar[1][1]-covar[0][0])**2 + 4*(covar[0][1])**2)

        a = np.sqrt(0.5*(covar[0][0] + covar[1][1] + SQRT))
        b = np.sqrt(0.5*(covar[0][0] + covar[1][1] - SQRT))

        self._plot_ellipse(centre_lon, centre_lat, a, b, theta, 1000, colour, label)

    def _plot_ellipse(self, centre_lon, centre_lat, a, b, theta, N, colour, label):
        AZ = np.linspace(0, 360, N)
        verts = []

        c_theta = np.cos(theta)
        s_theta = np.sin(theta)

        for az in AZ:
            _az = np.radians(az)

            c_az = np.cos(_az)
            s_az = np.sin(_az)

            this_x = a*c_az*c_theta - b*s_az*s_theta + centre_lon
            this_y = a*c_az*s_theta + b*s_az*c_theta + centre_lat

            verts.append(self(this_x, this_y))


        poly = Polygon(verts, ls='--', lw=1.5, ec=colour, fill=False, label=label, zorder=3)
        self._fig.gca().add_patch(poly)

    def plot_contour(self, points):
        from scipy.interpolate import griddata
        
        xs, ys, data = [], [], []
        for p in points:
            lat, lon = p.get_lat_lon()
            x, y = self(lon, lat)
            print(x,y)
            xs.append(x)
            ys.append(y)
            data.append(p.get_chisq())

        print(min(xs), max(xs), min(ys), max(ys))
            
        xi = np.linspace(min(xs), max(xs), 500)
        yi = np.linspace(min(ys), max(ys), 500)
        xi, yi = np.meshgrid(xi, yi)

        print(len(xs), len(ys), len(data))
        print(xi.shape, yi.shape)

        zi = griddata((np.array(xs), np.array(ys)), np.array(data), (xi, yi), method='linear')
        print(zi.shape)

        print(np.amin(zi), np.amax(zi), np.mean(zi))
              
        self.contour(xi, yi, zi)

        plt.show()

    def plot_contour_simple(self, points):
        xs, ys, data = [], [], []
        for p in points:
            lat, lon = p.get_lat_lon()
            xs.append(lon)
            ys.append(lat)
            data.append(p.get_chisq())

        #self.contour(np.array(xs), np.array(ys), np.array(data), tri=True, latlon=True)

        plt.tricontour(np.array(xs), np.array(ys), np.array(data))
       
        plt.show()
