"""Module containing the PeakDetectionWaveform (AKA `PDW`) class.

This module contains the implementation of the PeakDetectionWaveform class.
The PDW class is a convenient class that handles waveform data as they pass
through the geolocation algorithm.
"""

import matplotlib.pyplot as plt
import numpy as np


class PeakDetectionWaveform:
    """PeakDetectionWaveform class that holds data for an individual PDW event.

    This class bundles together important information about a PDW event into one
    class. Along with the raw waveform data, it calculates the normalised
    waveform and holds information regarding to the station it comes from. This
    class is designed to be passed around the geolocation algorithm.

    Attributes:
        station (:obj: `StationGeoPoint`): A StationGeoPoint class of the station
            that this PDW was received in.
        pdw_index (int): The integer index of this PDW from this station. This
            number is labelled `j' in the algorithm paper.
        start_time (float): The start time of this PDW i.e. the UNIX time that
            the first sample occurs.
        sample_time (float): The sampling time of this waveform (1/fs).
        stroke_index (int): The matched index of this PDW. This is set by the
            matching and geolocation algorithm. PDWs that share the same
            stroke_index are deemed to belong to the same lightning event. Known
            as `m' in the paper.
        raw_wvfm (:obj: `ndarray` of float): The raw waveform data from the
            detector for this PDW.
        normed_wvfm (:obj: `ndarray` of float): The normalised waveform of this
            PDW. This class automatically computes this from the raw_wvfm and
            sigma_k.
        y_rms (float): The root mean squared (RMS) of this waveform. This is
            computed internally by this class. Used to normalise the waveform.
        sigma (float): The normalised RMS of the background noise of the detector
            that this PDW is from. Used in the geolocation algorithm.
    """
    __slots__ = [
        'station', 'pdw_index', 'raw_wvfm', 'start_time', 'sample_time',
        'stroke_index', 'normed_wvfm', 'y_rms', 'sigma'
    ]

    def __init__(self, station, pdw_index, start_time, sample_time, wvfm,
                 sigma_k):
        """Initialises a PDW class.

        Args:
            station (:obj: `StationGeoPoint`): The station object from which
                this PDW was detected by.
            pdw_index (int): The integer index assigned to this PDW.
            start_time (float): The start time of this PDW.
            sample_time (float): The sampling time of this PDW.
            wvfm (:obj: `ndarray` of float): The raw waveform data of this PDW.
                Should be passed as a 1D numpy array but lists will be converted.
            sigma_k (float): The RMS of the raw background noise of the detctor
                from which this PDW originates.

        Raises:
            TypeError: If `wvfm` is not an array object (list or ndarray).
        """
        if type(wvfm) not in [list, np.ndarray]:
            raise TypeError(
                'Argument `wvfm` must be a list or numpy ndarray object.')
        if isinstance(wvfm, list):
            self.raw_wvfm = np.array(wvfm)
        else:
            self.raw_wvfm = wvfm
        self.station = station
        self.pdw_index = pdw_index
        self.start_time = start_time
        self.sample_time = sample_time
        self.stroke_index = -1
        self.normed_wvfm = None
        self.y_rms = None
        self.sigma = None
        self._norm_wvfm(sigma_k)
        
    def __lt__(self, other):
        return self.start_time < other.start_time

    def __len__(self):
        return len(self.raw_wvfm)

    def __str__(self):
        return '<Station: {}, Samples: {}, RMS: {:.3f}>'.format(
            self.get_station().get_id(), len(self.raw_wvfm), self.y_rms)

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, index):
        return self.normed_wvfm[index]

    def get_station_index(self):
        """Returns the station index from the station for this PDW."""
        return self.station.get_st_index()

    def get_station(self):
        """Returns the StationGeoPoint object associated with this PDW."""
        return self.station

    def get_pdw_index(self):
        """Returns the PDW index (j) for this PDW."""
        return self.pdw_index

    def get_stroke_index(self):
        """Returns the matched stroke index for this PDW."""
        return self.stroke_index

    def get_start_time(self):
        """Returns the start time of this PDW."""
        return self.start_time

    def get_rms(self):
        """Returns the RMS of this PDW."""
        return self.y_rms

    def get_sample_time(self):
        """Returns the sampling time of this PDW."""
        return self.sample_time

    def get_wvfm(self):
        """Returns the normalised waveform as an array for this PDW."""
        return self.normed_wvfm

    def get_raw_wvfm(self):
        """Returns the raw waveform as an array for this PDW."""
        return self.raw_wvfm

    def get_sigma(self):
        """Returns the normalised RMS of the station noise for this PDW."""
        return self.sigma

    def set_stroke_index(self, stroke_index):
        """Sets the matched stroke index for this PDW. Should be integer."""
        self.stroke_index = stroke_index

    def _norm_wvfm(self, sigma_k):
        """Private method that computes the normalised waveform and sigma."""
        self.y_rms = np.sqrt(np.mean(self.raw_wvfm**2))
        self.normed_wvfm = self.raw_wvfm / self.y_rms
        self.sigma = sigma_k / self.y_rms

    def as_dict(self):
        """Method that converts the PDW into a dict to pass into C++ backend.

        This method converts relevant parts of the PDW class into a dictionary
        so that it can be passed into the C++ backend.
        """
        st_lat, st_lon = self.station.get_lat_lon()
        return {'start_time': float(self.start_time),
                'sigma': float(self.sigma),
                'st_lat': float(st_lat),
                'st_lon': float(st_lon),
                'wvfm': [float(i) for i in self.normed_wvfm],
                'sample_time': float(self.sample_time)}

    def show_pdw(self, plot_raw_wvfm=False):
        """Method to plot the waveform of this PDW using matplotlib.

        Args:
            plot_raw_wvfm (bool): Flag that enables the raw waveform to
                be plotted when it is True. The normalised waveform is
                plotted when this is False. Defaults to False.
        """
        if not plot_raw_wvfm:
            plt.plot(self.normed_wvfm, 'm-')
            y_label = r'$\tilde{y}(i,j,k)$ [a.u]'
        else:
            plt.plot(self.raw_wvfm, 'm-')
            y_label = r'Amplitude (raw) [a.u]'
        plt.xlabel(r'Sample $i$')
        plt.ylabel(y_label)
        plt.show()
