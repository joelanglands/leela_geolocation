"""Module for FineChiMinimiser class to perform the fine minimisation.

Implementation of the FineChiMinimiser class. This class is fairly bulky as it
uses a lot of class attributes to make it easy for the migrad function to run.
The most useful methods are the `minimise` method which actually runs the fine
minimisation. The `get_p_value` method returns the p value for this minimisation
which indicates the likelihood of the input PDWs to be from the same event.
The `get_cov` method returns the 68% confidence level (CL) covariance matrix
which can be passed straight into the `plot_error_ellipse` method of the
`MapPlotting` class to plot the error ellipse for the event. The 95% CL ellipse
covariance matrix can also be accessed with the `get_95_cov` method.
"""

import iminuit as im
import numpy as np
import scipy.stats as stats

from geolocation.geoclasses import BaseGeoPoint
from geolocation.geoutils import calc_t0_shift, calc_distance
from geolocation.gl_cpp import geolocation_cpp as gl_cpp


class FineChiMinimiser:
    """A class to handle the fine minimisation algorithm using minuit.

    This class is rather large and initialises a lot of internal attributes
    that the end user should not worry about. A lot of attributes are a matter
    of making it convenient for the minuit package which actually does the work.
    Attributes of this class should be considered private and it should be
    interacted with using the the most important class methods.
    """
    def __init__(self, pdws, seed_point):
        """Initialises the FineChiMinimiser class.

        Args:
            pdws (:obj:`list` of :obj:`PeakDetectionWaveform`): The list of PDWs
                that the geo location fix should be calculated for.
            seed_point (:obj:`BaseGeoPoint` or :obj:`CGridGeoPoint`): The seed
                point from the coarse grid minimisation for these PDWs.
        """
        pdws.sort()
        self.ref_pdw = pdws[0]
        self.ref_station = self.ref_pdw.get_station()
        self.other_pdws = pdws[1:]
        self.seed_point = seed_point
        self.t_samp = self.ref_pdw.get_sample_time()
        self.N_samp = len(self.ref_pdw)
        self.ndof = self.N_samp * len(self.other_pdws) - len(self.other_pdws) - 2
        self.null_ndof = self.N_samp * (len(self.other_pdws) + 1)
        self.averaged_wvfm = []

        #debugging quantities
        self.minos_status = (False, False)
        self.minos_errors = None

        #ref and other_pdws in correct format for C++ min function
        self.ref_pdw_cpp = self.ref_pdw.as_dict()
        self.other_pdws_cpp = [other.as_dict() for other in self.other_pdws]

    def minimise(self, dlayerheight=False, print_level=0):
        """Class method that runs the minimisation for this group of PDWs.

        This class method actually initialises minuit and uses MIGRAD to perform
        the minimisation. This class also attempts to run the MINOS error
        routine if the minimum is valid.

        Args:
            dlayerheight (bool): If True, the height of the ionosphere will be
                included in the fit. The minimisation will then attempt to
                calculate a bestfit height for the dlayer height in km.
                Experimental. Defaults to False.
            print_level (int): Sets the print level of migrad. Should be set to
                either 0, 1 or 2. 0 is quiet. 1 prints out information from the
                fit. 2 prints debug messages. Defaults to 0.
        
        Returns:
           tuple (float, float, float): Returns tuple with resulting latitude,
                longitude and dlayer height in that order.
        """
        seed_lat, seed_lon = self.seed_point.get_lat_lon()
        ERROR = 3.0
        lat_down = seed_lat - ERROR
        lat_up = seed_lat + ERROR
        lon_down = seed_lon - ERROR
        lon_up = seed_lon + ERROR

        if dlayerheight is True:
            _dlayer = 70
            dlayer_lim = (0, 100)
        else:
            _dlayer = 0
            dlayer_lim = (0, 0)

        self.minimiser = im.Minuit(self._min_func,
                                   lat=seed_lat,
                                   lon=seed_lon,
                                   dlayer=_dlayer,
                                   limit_lat=(lat_down, lat_up),
                                   limit_lon=(lon_down, lon_up),
                                   limit_dlayer=dlayer_lim,
                                   print_level=print_level,
                                   errordef=2.31,
                                   fix_dlayer=not dlayerheight,
                                   pedantic=False)

        self.minimiser.migrad(precision=0.000001)

        self.valid_min = self.minimiser.get_fmin()['is_valid']

        if self.minimiser.get_fmin()['is_valid'] is True:
            try:
                self.minimiser.minos()
                self.minos_errors = self.minimiser.get_merrors()
                self.minos_status = (self.minos_errors['lat']['is_valid'],
                                     self.minos_errors['lon']['is_valid'])
            except ValueError:
                self.minos_status = (False, False)
            except RuntimeError:
                self.minos_status = (False, False)

        self.min_chi = self.minimiser.get_fmin()['fval']

        self.result_lat = self.minimiser.values['lat']
        self.result_lon = self.minimiser.values['lon']
        self.result_dlayer = self.minimiser.values['dlayer']

        if self.minimiser.get_fmin()['hesse_failed'] is False:
            self.covar_65 = self.minimiser.matrix()
        else:
            self.covar_65 = None

        try:
            self.minimiser.set_errordef(5.99)
            self.minimiser.hesse()
            self.covar_95 = self.minimiser.matrix()
        except RuntimeError:
            self.covar_95 = None

        self.min_point = BaseGeoPoint(self.result_lat, self.result_lon)
        self._calc_av_wvfm_null_p()
        
        return self.result_lat, self.result_lon, self.result_dlayer

    def _min_func(self, lat, lon, dlayer):
        result = gl_cpp.fine_min_func(self.ref_pdw_cpp,
                                      self.other_pdws_cpp,
                                      lat,
                                      lon,
                                      dlayer)
        return result

    def is_valid(self):
        """Returns True if the minimum is valid and False if its invalid."""
        return self.valid_min

    def hesse_failed(self):
        """Returns False if hesse fails and True if it is valid."""
        return self.minimiser.get_fmin()['hesse_failed']

    def get_chi(self):
        """Returns the minimum chi squared value for this fit."""
        return self.min_chi

    def get_result(self):
        """Returns the result latitude, longitude and dlayer height."""
        return self.result_lat, self.result_lon, self.result_dlayer

    def get_result_point(self):
        """Returns a BaseGeoPoint object of the resulting location."""
        return self.min_point

    def get_cov(self):
        """Returns the fit covariance matrix which forms the 65% CL ellipse

        Raises:
            ValueError: Raised if Hesse fails to obtain the covariance matrix.
        """
        if self.covar_65 is not None:
            return self.covar_65
        else:
            raise ValueError('Hesse failed so there is no covariance matrix!')

    def get_95_cov(self):
        """Returns the fit covariance matrix which forms the 95% CL ellipse.

        Raises:
            ValueError: Raised if Hesse fails to obtain the covariance matrix.
        """
        if self.covar_95 is not None:
            return self.covar_95
        else:
            raise ValueError('Hesse failed so there is no covariance matrix!')

    def get_error_ellipse_params(self, cov_95=False):
        """Returns the area and size of the error ellipse.

        This function calculates the lengths of the major and minor axis of the
        error ellipse. It also calculates the area of the error ellipse.

        Args:
            cov_95 (bool): Returns parameters of 95% CL ellipse if True. Returns
                parameters of the 65% CL ellipse if False. Defaults to False.

        Returns:
            tuple (float, float, float): Major axis in metres, minor axis in
                metres and the area of the ellipse in metres squared.
        Raises:
            ValueError: If either of the covariance matrices are `None`. Which
                is due to the hesse routine failing.
            ValueError: If for whatever reason the calculated minor/major axes
                are NANs which could be due to an invalid covariance matrix.
        """
        if cov_95 is False:
            covar = self.covar_65
        else:
            covar = self.covar_95
        if covar is None:
            raise ValueError('No valid covariance matrix for this fit!')
        
        sqrt = np.sqrt((covar[1][1] - covar[0][0])**2 + 4
                       * (covar[0][1])**2)
        theta = 0.5 * np.arctan(2 * covar[0][1]
                                / (covar[1][1] - covar[0][0]))

        a = np.sqrt(0.5 * (covar[0][0] + covar[1][1] + sqrt))
        b = np.sqrt(0.5 * (covar[0][0] + covar[1][1] - sqrt))

        maj_lon = a * np.cos(theta) + self.result_lon
        maj_lat = a * np.sin(theta) + self.result_lat
        major_axis = calc_distance(BaseGeoPoint(maj_lat,
                                                maj_lon),
                                   BaseGeoPoint(self.result_lat,
                                                self.result_lon))

        min_lon = -b * np.sin(theta) + self.result_lon
        min_lat = b * np.cos(theta) + self.result_lat
        minor_axis = calc_distance(BaseGeoPoint(min_lat,
                                                min_lon),
                                   BaseGeoPoint(self.result_lat,
                                                self.result_lon))
        if np.any(np.isnan([major_axis, minor_axis])):
            raise ValueError('Invalid covariance matrix!')

        return (major_axis, minor_axis, np.pi * major_axis * minor_axis)

    def get_minos_status(self):
        """Returns minos status. True if valid False if failed."""
        return np.all(self.minos_status)

    def get_minos_errors(self):
        """Returns the errors given by MINOS."""
        return self.minos_errors

    def get_p_value(self):
        """Returns the p value for this event."""
        chi_func = stats.chi2(self.ndof)
        self.p_fine = 1 - chi_func.cdf(self.min_chi)
        return self.p_fine

    def get_null_p_value(self):
        """Returns the null p value for this event."""
        return self.p_null

    def get_mean_wvfm(self):
        """Returns the averaged waveform for this event."""
        return self.averaged_wvfm
    
    def _calc_av_wvfm_null_p(self):
        """Calculates the averaged waveform and the null p value together."""
        _sum = 0
        for i in self.ref_pdw.get_wvfm():
            _sum += i**2 / (self.ref_pdw.get_sigma()**2)
            
        self.averaged_wvfm = self.ref_pdw.get_wvfm()
        for other in self.other_pdws:
            t_shift = calc_t0_shift(self.ref_pdw.get_start_time(),
                                    self.ref_station,
                                    other.get_station(),
                                    self.min_point,
                                    self.result_dlayer)

            alpha = self._calc_alpha(t_shift, other.get_start_time())
            i_prime = int(np.floor(alpha))
            i = 0
            sigma = other.get_sigma()**2
            aux_arr = []
            while i < len(self.ref_pdw):
                if i_prime + 1 < len(other) and i_prime >= 0:
                    other_y = self._interpolate_wvfm(other[i_prime],
                                                     other[i_prime + 1],
                                                     alpha,
                                                     i_prime)
                else:
                    other_y = 0

                aux_arr.append(other_y)
                _sum += (other_y**2) / sigma
                i += 1
                i_prime += 1
            self.averaged_wvfm += np.array(aux_arr)
            
        self.averaged_wvfm /= (len(self.other_pdws) + 1)
        chi_func = stats.chi2(self.null_ndof)
        self.p_null = 1 - chi_func.cdf(float(_sum))

    def _calc_alpha(self, t_shift, t_zero):
        return (t_shift - t_zero)/self.t_samp

    def _interpolate_wvfm(self, low_p, hi_p, alpha, i_prime):
        if i_prime < 0 or i_prime > (self.N_samp - 1):
            low_p = 0
        if i_prime < -1 or i_prime > (self.N_samp - 2):
            hi_p = 0

        if np.floor(alpha) == 0:
            mult = alpha
        elif np.floor(alpha) > 0:
            mult = np.remainder(alpha, np.floor(alpha))
        elif alpha < 0 and alpha > -1:
            mult = 1 - abs(alpha)
        elif np.floor(alpha) < 0:
            mult = np.remainder(alpha, np.ceil(alpha)) + 1

        return low_p + (hi_p - low_p)*mult
