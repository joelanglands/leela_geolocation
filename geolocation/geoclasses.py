"""Module containing basic classes to handle geographic locations on the Earth.

This module contains convenient classes that encode geospatial data i.e.
latitude and longitudes along with other useful information. These classes are
used throughout the geolocation code.

Classes:
    BaseGeoPoint: Base class that represents a point on the Earth with
      its latitude and longitude.
    CGridGeoPoints: Inherits from BaseGeoPoint. Also holds chisq and grid index
      value for use with the coarse grid algorithm.
    StationGeoPoint: Inherits from BaseGeoPoint. Also holds indentifiers
      relating to the LEELA station it represents.
"""

import numpy as np


class BaseGeoPoint:
    """Base class representing a point on the Earth.

    Attributes:
        latitude (float): The latitude of this point.
        longitude (floa): The longitude of this point.
    """
    __slots__ = ['latitude', 'longitude']

    def __init__(self, latitude=0, longitude=0):
        """Constructs a BaseGeoPoint with a given latitude and longitude.

        Args:
            latitude (float): The latitude of this point.
            longitude (float): The longitude of this point.

        """
        self.latitude = np.float64(latitude)
        self.longitude = np.float64(longitude)

    def get_lat_lon(self):
        """Returns the latitude and longitude as a tuple in that order."""
        return self.latitude, self.longitude

    def __str__(self):
        return '<BaseGeoPoint @ ({:.6f}, {:.6f})>'.format(
            self.latitude, self.longitude)


class CGridGeoPoint(BaseGeoPoint):
    """Geographic point class for points on a coarse grid.

    Attributes:
        latitude (float): The latitude of this point.
        longitude (float): The longitude of this point.
        grid_index (int): The coarse grid index of this point.
        chisq (float): The value of chi squared at this point.
    """
    __slots__ = ['grid_index', 'chisq']

    def __init__(self, grid_index, latitude=0, longitude=0, chisq=None):
        """Initialises a point on the coarse grid.

        Args:
            grid_index (int): Coarse grid index of the point.
            latitude (float): The latitude of this point.
            longitude (float): The longitude of this point.
            chisq (float): The initial chi square value of this point. Defaults
              to None.

        """
        super().__init__(latitude, longitude)
        self.grid_index = grid_index
        self.chisq = chisq

    def get_index(self):
        """Returns the grid index of this point."""
        return self.grid_index

    def get_chisq(self):
        """Returns the chi squared value of this point."""
        return self.chisq

    def set_chisq(self, chisq):
        """Sets the chi squared value of this point."""
        self.chisq = chisq

    def __str__(self):
        return '<CGridPoint @ ({:.6f}, {:.6f}), l={}, chisq={}>'.format(
            self.latitude, self.longitude, self.grid_index, self.chisq)


class StationGeoPoint(BaseGeoPoint):
    """Geographic point class for LEELA stations

    Attributes:
        latitude (float): The latitude of this point.
        longitude (float): The longitude of this point.
        station_number (int): The integer identifier of this station.
        station_id (str): The string identifier of this station.
    """
    __slots__ = ['station_number', 'station_id']

    def __init__(self, station_number, latitude, longitude, ID='UKWN'):
        """Initialises a station geopoint with a station number, lat/lon and ID.

        Args:
            station_number (int): An integer identifier for this station. When
              handling multiple stations, these should be different for
              each station.
            latitude (float): The latitude of this station.
            longitude (float): The longitude of this station.
            ID (str): A string identifier for this station.
              Four capital letters is preferred. Defaults to `UKWN'.

        """
        super().__init__(latitude, longitude)
        self.station_number = station_number
        self.station_id = ID

    def get_st_index(self):
        """Returns the integer identifier of this station."""
        return self.station_number

    def get_id(self):
        """Returns the string identifier of this station."""
        return self.station_id

    def __str__(self):
        return "<Station #{} '{}' @ ({:.6f}, {:.6f})>".format(
            self.station_number, self.station_id, self.latitude,
            self.longitude)
