"""Module containing core functions for the geolocation algorithm.

This module contains core functions and attributes for use with the geolocation
algorithm. Some are python wrappers for the C++ backend library.

Attributes:
    SPEED_OF_LIGHT (float): The speed of light in vacuum in metres per second.
    preset_map_settings (dict): Preset settings for use with the coarse grid
      or the map plotting tools.
"""

import itertools as it

import numpy as np

from geolocation.gl_cpp import geolocation_cpp as gl_cpp
from geolocation.geoclasses import CGridGeoPoint

SPEED_OF_LIGHT = 299792458.0 #metres per second -- exactly (in vacuum)

preset_map_settings = {
    'EU': {
        'llclon': -25,
        'llclat': 25,
        'urclon': 45,
        'urclat': 70
    },
    'UK': {
        'llclon': -12,
        'llclat': 50,
        'urclon': 5,
        'urclat': 60
    },
    'GLOBE': {
        'llclon': -175,
        'llclat': -87,
        'urclon': 175,
        'urclat': 87
    }
}

def calc_distance(point1, point2, dlayerhop=None):
    """Calculates light travel distance, in metres, between two points.

    This function calculates the great circle distance between two points
    on the surface of the Earth if the `dlayerhop` option is not used. Otherwise
    it will take into account a reflection off the Earth's ionosphere, the
    height of which should be passed in the dlayerhop option.

    Args:
        point1 (:obj:`BaseGeoPoint`): First point of interest on the Earth.
        point2 (:obj:`BaseGeoPoint`): Second point of interest on the Earth.
        dlayerhop (int or float, optional): The height of the ionosphere off
          of which the signal will reflect. Defaults to None, in which case
          it returns the great circle distance.

    Returns:
        float: The light travel distance between point1 and point2 in metres.
    """
    lat1, lon1 = point1.get_lat_lon()
    lat2, lon2 = point2.get_lat_lon()
    if isinstance(dlayerhop, (int, float)):
        return gl_cpp.calc_distance(lat1, lon1, lat2, lon2, dlayerhop)
    return gl_cpp.calc_distance(lat1, lon1, lat2, lon2, 0)

def calc_light_time(point1, point2, dlayerhop=None):
    """Calculates light travel time between two points on the Earth.

    Usage of dlayerhop is identical to `calc_distance` function.

    Args:
        point1 (:obj:`BaseGeoPoint`): First point of interest on the Earth.
        point2 (:obj:`BaseGeoPoint`): Second point of interest on the Earth.
        dlayerhop (int or float, optional): The height of the ionosphere.
          Defaults to None, in which case the great circle distance travel time
          is returned.

    Returns:
        float: The light travel time between point1 and point2.
    """
    return np.float128(calc_distance(point1, point2, dlayerhop)/SPEED_OF_LIGHT)

def calc_t0_shift(ref_start_time, ref_station, other_station, grid_point, dlayerhop=None):
    """Returns the shifted start time."""
    dt_ref = calc_light_time(ref_station, grid_point, dlayerhop)
    dt_other = calc_light_time(other_station, grid_point, dlayerhop)
    return ref_start_time + dt_other - dt_ref

def calc_ts_index(t0_shift, t_zero, t_samp):
    """Returns the time shifted sample index."""
    return int(np.floor((t0_shift - t_zero)/t_samp)) #np.floor doesn't return integer

def calc_t_max(detectors, dlayerhop=0):
    """Returns the maximum light travel time across a given detector network.

    This function takes a list of StationGeoPoint objects and returns the
    maximum light travel time across the network by taking the max light travel
    time for every combination of two detectors.

    Args:
        detectors (:obj:`list` of :obj:`StationGeoPoint`): A list of
          StationGeoPoint objects for all the detectors in use.
        dlayerhop (float, optional): The height of the ionosphere. Defaults to
          None, in which case great circle distances are used.
    """
    _times = []
    for pair in it.combinations(detectors, 2):
        _times.append(calc_light_time(pair[0], pair[1], dlayerhop=dlayerhop))
    return max(_times)


def coarse_grid(pdws,
                setting='EU',
                lat_step=0.1,
                lon_step=0.1,
                dlayer=0,
                p_tested=False):
    """Calculates the value of chi squared over a coarse grid.

    This function calculates the chi squared value at every point in a grid of
    points. It then returns the minimum point for the fine minimisation.

    Args:
        pdws (:obj:`list` of :obj:`PeakDetectionWaveform`): A list of PDW
            classes, maximum of one from each detector, of which to locate the
            origin.
        setting (str, optional): Which preset map setting to use -- `EU`, `UK`
             or `GLOBE`. This defines the boundaries of the coarse grid.
             Defaults to `EU`.
        lat_step (float, optional): The step size in degrees, in latitude to use
            for the grid. Defaults to 0.1 degree.
        lon_step (float, optional): The step size in degrees, in longitude to use
            for the grid. Defaults to 0.1 degree.
        dlayer (float, optional): The height of the ionosphere d-layer in
            KILOMETRES. Defaults to 0.
        p_tested (bool, optional): Used for debugging. Defaults to False. If
            True this function will also return the amount of points actually
            tested from the hyperbolic cut on the grid.

    Returns:
        CGridGeoPoint: The point on the coarse grid where chi squared is
            minimum.
        int: If p_tested is set to True it will also return the number of points
            where chi squared has been calculated.
    """
    cgrid_dict = {
        'lat_start': preset_map_settings[setting.upper()]['llclat'],
        'lat_end': preset_map_settings[setting.upper()]['urclat'],
        'lat_step': lat_step,
        'lon_start': preset_map_settings[setting.upper()]['llclon'],
        'lon_end': preset_map_settings[setting.upper()]['urclon'],
        'lon_step': lon_step
    }
    pdws.sort()

    result_lat, result_lon, chi_min, p_test = gl_cpp.coarse_grid(
        cgrid_dict, pdws[0].as_dict(), [j.as_dict() for j in pdws[1:]], dlayer)

    if not p_tested:
        return CGridGeoPoint(1, result_lat, result_lon, chisq=chi_min)
    return CGridGeoPoint(1, result_lat, result_lon, chisq=chi_min), p_test

def coarse_grid_stage_zero(pdws,
                           guess_loc_1,
                           guess_loc_2,
                           step=0.1,
                           s0_err=10,
                           dlayer=0,
                           p_tested=False):
    """Calculates the value of chi squared over a grid with mulilat input.

    Similar to the coarse grid function, however this uses two guess locations
    from the multilateration to form two smaller coarse grids to calculate over.
    (See README for details).

    Args:
        pdws (:obj:`list` of :obj:`PeakDetectionWaveform`): A list of PDW
            classes, maximum of one from each detector, of which to locate the
            origin.
        guess_loc_1 (:obj:`BaseGeoPoint`): Guess location one from the
            multilateration function.
        guess_loc_2 (:obj:`BaseGeoPoint`): The other guess location.
        step (float, optional): The latitude and longitude step in degrees to
            use for the sub grids. Defaults to 0.1 degrees.
        s0_err (int, optional): The number of degrees around the guess location
            to search. Defaults to 10 degrees. This means the default forms two
            grids about the guess locations that are 20x20 degrees.
        dlayer (float, optional): The height of the ionosphere d-layer in
            KILOMETRES. Defaults to 0.
        p_tested (bool, optional): Used for debugging. Defaults to False.
            If True this function will also return the amount of points
            actually tested from the hyperbolic cut on the grid.

    Returns:
        CGridGeoPoint: The point on the coarse grid where chi squared is
            minimum.
        int: If p_tested is set to True it will also return the number of points
            where chi squared has been calculated.
    """
    gl1_lat, gl1_lon = guess_loc_1.get_lat_lon()
    gl2_lat, gl2_lon = guess_loc_2.get_lat_lon()

    cgrid_dict_1 = {'lat_start': int(gl1_lat) - s0_err,
                    'lat_end': int(gl1_lat) + s0_err,
                    'lat_step': step,
                    'lon_start': int(gl1_lon) - s0_err,
                    'lon_end': int(gl1_lon) + s0_err,
                    'lon_step': step}

    cgrid_dict_2 = {'lat_start': int(gl2_lat) - s0_err,
                    'lat_end': int(gl2_lat) + s0_err,
                    'lat_step': step,
                    'lon_start': int(gl2_lon) - s0_err,
                    'lon_end': int(gl2_lon) + s0_err,
                    'lon_step': step}

    pdws.sort()

    result_lat, result_lon, chi_min, p_test = gl_cpp.coarse_grid_stage_zero(
        cgrid_dict_1, cgrid_dict_2, pdws[0].as_dict(),
        [j.as_dict() for j in pdws[1:]], dlayer)

    if not p_tested:
        return CGridGeoPoint(1, result_lat, result_lon, chi_min)
    return CGridGeoPoint(1, result_lat, result_lon, chi_min), p_test
