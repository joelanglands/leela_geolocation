SRCDIR = $(addprefix $(PWD), /geolocation/gl_cpp)

default:
	$(MAKE) -C $(SRCDIR)

clean:
	$(MAKE) -C $(SRCDIR) clean

.PHONY: default clean
