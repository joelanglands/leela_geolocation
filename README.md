# LEELA Geolocation Framework

##Introduction

This code is a prototype software framework for locating the origin of lightning strikes from the LEELA network. It follows the algorithm written by Dan Tovey which can be found in the file `papers/leela_algorithm.pdf`. The code is predominantly written in python 3.X with some C++11 backend to enable fast computation. All the code the user should interact with is written in python for ease of use.

The directory `papers/` also contains some other useful documentation regarding different parts of the code. The minuit textbook pdf refers to the actual C++ version of minuit from CERN. Although this is not useful for the python port used here (see next section for iminuit documentation), it does explain some of the underlying mechanics and usage of the package.


## Requirements and Installation

The code is intended to be easy to run on a UNIX operating system within a conda environment. For the python side, the required core packages are:

  * [numpy](https://numpy.org/)
  * [scipy](https://www.scipy.org/)
  * [matplotlib](https://matplotlib.org/)
  * [iminuit](https://iminuit.readthedocs.io/en/stable/)

Which are all readily available through conda or pip.

The framework has an inbuilt map plotting class which currently uses the [`Basemap`](https://matplotlib.org/basemap/) package, although this will soon be deprecated so this module may be ported to [`Cartopy`](https://scitools.org.uk/cartopy/docs/latest/).

The C++ needs to be compiled before use. The package [`pybind11`](https://pybind11.readthedocs.io/en/stable/) is used to build C++ extension libraries for the python framework. It should be easily installed using conda:

    $ conda install -c conda-forge pybind11
You may also need to install the `python-dev` package which is readily available through package managers. If you are on a linux system, you will most likely have `make` already installed and so you can run

    $ make
in the top level directory to compile the C++ code using GCC. To find other ways of compiling the code check out the `pybind11` documentation, linked above, for more information. The C++ code should compile into a shared object file called `geolocation_cpp.so`.

## About the Code

The algorithm itself can be found in the `papers/` directory as said above. The key data that the algorithm works with is the 'Peak Detection Waveform' or PDW for short. A PDW is the waveform produced in a detector by a lightning strike. Data from LEELA is comprised of 'chunks' of 1024 samples. The PDWs should be extracted from the chunks and put into the PDW class properly with a good trigger system that actually extracts the whole waveform and nothing else. This would need to be written by the user for full usage of this package as I did not have time myself to have a proper go at it. An example of how to use the PDW class is shown in the module `gl_testing/test_core.py` in the `load_test_waveforms()` and `load_test_data()` functions.

The framework itself lives in the `geolocation/` directory.  It already exposes the key components so that they should be useable by simply importing the `geolocation` module. An example script in the top level directory called `example_geolocation.py` shows a couple of examples of how to use the code. Hopefully, the mathematical parts of the code should be fairly easy to follow with the algorithm paper in front of you.

### Test data

The directory `gl_testing/` contains a few test scripts, some test data and a very basic (and bad) attempt at PDW extraction code which could be termed a 'trigger'. In this directory there is another directory called `test_data` which contains the test data in `.npy` files to be opened with `numpy`. The test data contains a file called `test_waveforms.npy` which contains five example chunks with five matched PDWs within them. There are also five other files from five different stations which contain one minute of LEELA data.

The test data is in peak centred format, where the central sample of the chunk (sample 511) is the peak of the signal envelope for that waveform. A PDW can be simple and just one of these peak centred waveforms or it can be a chain of them. The data is stored in a structured numpy array and important parameters and the waveform data itself can be accessed using keys in the same way values are accessed in a dictionary. For example, if you were to load a file using `data = np.load(filename)`, then the waveform data for the first 1024 sample chunk can be accessed using `data[0]['wvfmdata']`. The peak time, sample interval and detector identifier for a chunk can be accessed with the keys `peaktime`, `sampleinterval` and `nodename` respectively. You can check out the `test_core.py` script for usage.

For more information about the data format and to see some plots of the data you can look at the beginning section of the jupyter notebook [here](https://github.com/JoeLanglands/leela-ml/blob/master/leela_VAE.ipynb).

## Author Information

Algorithm - Dan Tovey

Software initial author and maintainer - Joe Langlands (joe.langlands@gmail.com)
Feel free to email me with any questions you have and I will aim to get back to you as soon as I can.




